#include "invoice.h"
#include <string>

using namespace std;

void main() {
	setlocale(LC_ALL, "Russian");
	printf("������� ������ a\n");
	Invoice a = {"33233-99","�������� ������� ��������",23,450};
	
	printf("�������� ������� ������� - %s\n", a.getArticul());
	printf("�������� �������� ������� - %s\n", a.getDescribe());
	printf("�������� ���-�� - %i\n", a.getCounts());
	printf("�������� ���� ������ ������� - %lf\n", a.getPrice());
	printf("�������� ����� ����� ��������� - %i\n", a.getInvoiceAmount());
	printf("�������� ����� ���-�� ��������� - %i\n", a.getGlobalCount());
	printf("������ ���������:\n\n");
	a.printInvoice();

	printf("������� ������������ ������ b\n");
	Invoice * b = new Invoice;
	b->setArticul("11243-00");
	b->setDescribe("������������ ������");
	b->setCounts(244);
	b->setPrice(125);
	printf("�������� ������� ������� - %s\n", b->getArticul());
	printf("�������� �������� ������� - %s\n", b->getDescribe());
	printf("�������� ���-�� - %i\n", b->getCounts());
	printf("�������� ���� ������ ������� - %lf\n", b->getPrice());
	printf("�������� ����� ����� ��������� - %i\n", b->getInvoiceAmount());
	printf("�������� ����� ���-�� ��������� - %i\n", b->getGlobalCount());
	printf("������ ���������:\n\n");
	b->printInvoice();

	printf("������� 3 �������� � ����������� �������\n");
	Invoice invarr[3] = { Invoice("14432-00","���������",2221,80),
		Invoice("142442-00","�����",241,15),
		Invoice("142443-00","������",21,120) };
		printf("�������� � ���� ��������� \n");

		for (int i = 0; i < 2; i++)
			invarr[i].printInvoice();
	
		printf("������� 3 �������� � ������������ �������\n");
		Invoice * invarr2 = new Invoice[3];
		printf("��������� �������� ������� � �������� ���������\n");
		char tmp_art[20] = "15233-0";
		char tmp_desc[100] = "���� ����� ";
		int art_lengch = strlen(tmp_art);
		int desc_lengch = strlen(tmp_desc);
		for (int i = 0; i < 3; i++) {
			tmp_art[art_lengch] = (i + 1) + '0';
			tmp_art[art_lengch+1] = 0;
			tmp_desc[desc_lengch] = (i + 1) + '0';
			tmp_desc[desc_lengch + 1] = 0;
			invarr2[i].setArticul(tmp_art);
			invarr2[i].setDescribe(tmp_desc);
			invarr2[i].setCounts(250);
			invarr2[i].setPrice(300);
			invarr2[i].printInvoice();
		}

}