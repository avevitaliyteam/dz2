#include <string>

class Invoice
{
private:
	char articul[20]; // ������� �������
	char describe[100]; // �������� �������
	int counts; // ���-�� �������
	double price; // ���� �������
	int artc_number; // ��������� ���������
	static int global_count; // ���������� ������� �������

public:
	Invoice();
	Invoice(char * ar, char * desc, int cnt, double pr);
	~Invoice();
	
	inline void setCounts(int);
	inline int getCounts();

	inline void setPrice(double);
	inline double getPrice();

	inline void setArticul(char *);
	inline void setDescribe(char *);

	inline char * getArticul();
	inline char * getDescribe();

	inline int getInvoiceAmount();
	inline int getGlobalCount();

	void printInvoice();

};


inline void Invoice::setCounts(int cnt) {
	counts = cnt;
}


inline int Invoice::getCounts() {
	return counts;
}

inline void Invoice::setPrice(double pr) {
	price = pr;
}


inline double Invoice::getPrice() {
	return price;
}



inline char *Invoice::getArticul() {
	return articul;
}



inline char * Invoice::getDescribe() {
	return describe;
}


inline void Invoice::setArticul(char *ar) {
	strcpy(articul, ar);
}

inline void Invoice::setDescribe(char *desc) {
	strcpy(describe, desc);
}

inline int Invoice::getInvoiceAmount() {
	return (int)counts*price;
}

inline int Invoice::getGlobalCount() {
	return global_count;
}